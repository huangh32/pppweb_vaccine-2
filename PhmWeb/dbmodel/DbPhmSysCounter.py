from PhmWeb.utils.dbconfig import get_PHM_db


class DbPhmSysCounter:
    def __init__(self):
        self.description = 'get sequence id'

    def get_next_sequence_value(self,seq_id):
        if seq_id == 'onlineid':
            return self.get_next_online_id()
        elif seq_id == 'userid':
            return self.get_next_user_id()
        return 100

    def get_next_online_id(self):
        zw = get_PHM_db()
        zw['phm_sys_Counter'].update({'_id':'onlineid'}, {"$inc":{"sequence_value":1}})
        rr = list(zw['phm_sys_Counter'].find({'_id': 'onlineid'}))
        return rr[0]['sequence_value']

    def get_next_user_id(self):
        zw = get_PHM_db()
        zw['phm_sys_Counter'].update({'_id':'userid'}, {"$inc":{"sequence_value":1}})
        rr = list(zw['phm_sys_Counter'].find({'_id': 'userid'}))
        return rr[0]['sequence_value']


