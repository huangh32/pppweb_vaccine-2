"""PhmWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import manage
# from django.urls import path
from django.conf.urls import url
from django.views.generic.base import RedirectView
from django.conf import settings
from django.views import static
from docs import api_docs_view
from root import root_view
from somos import s1_landing_view, s2_sites_view, s3_register_view, s4_confirm_view, s5_appoint_view, s6_search_view, \
    s7_results, s_login, s0_appt_lab_history, s_logout, s_recover, s_signup_view, s8_test, email_verify_view, s9_subscribe_view
from api import appointment_api_view

urlpatterns = [
    url(r'^$', root_view.index),
    url(r'^favicon\.ico$', RedirectView.as_view(url=r'static/images/favicon.ico')),
    # default page

    # api 接口
    # url(r'^s/(?P<shorten_key>.+)$', schedule_view.visit_by_shorten_key, name='portal_schedule_view_p'),

    # url(r'^dev/test/', testview.test, name='devtest'),
    url(r'p/history/$', s0_appt_lab_history.index, name='s0_history'),
    url(r'p/covid$', s1_landing_view.index, name='s1_landing_view'),
    url(r'p/covid/$', s1_landing_view.index, name='s1_landing_view2'),
    url(r'p/callcenter/$', s1_landing_view.call_center, name='s1_landing_callcenter_view2'),
    url(r'p/sites/$', s2_sites_view.index, name='s2_sites_view'),
    url(r'p/register/$', s3_register_view.index, name='s3_register_view'),
    url(r'p/confirm/$', s4_confirm_view.index, name='s4_confirm_view'),
    url(r'p/appoint/$', s5_appoint_view.index, name='s5_appoint_view'),
    url(r'p/search/$', s6_search_view.index, name='s6_search_view'),
    url(r'p/results/$', s7_results.index, name='s7_results'),
    url(r'p/test/$', s8_test.index, name='s8_test'),
    url(r'p/subscribe/$', s9_subscribe_view.index, name='subscribe'),

    url(r'p/login/$', s_login.index, name='s_login'),
    url(r'p/logout/$', s_logout.index, name='s_logout'),
    url(r'p/recover/$', s_recover.index, name='s_recover'),
    url(r'p/signup/$', s_signup_view.index, name='s_signup'),
    url(r'p/emailverify/$', email_verify_view.index, name='email_verify'),

    url(r'p/api/apt/$', appointment_api_view.index, name='api_apt'),
    url(r'^p/static/(?P<path>.*)$', static.serve, {'document_root': settings.STATIC_ROOT}, name='static'),
]

urlpatterns += (
    url(r'^p/api/$', api_docs_view.index),
    url(r'^p/api/docs$', api_docs_view.docs),
    url(r'^p/api/docs/(?P<json>\w+)/$', api_docs_view.module),
)
