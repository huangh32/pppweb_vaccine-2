
from PhmWeb.utils.dbconfig import sql_ec_db


class CommunityService:
    def __init__(self, request):
        self._req = request
        self._domain = ("" + request.META['HTTP_HOST']).lower() if 'HTTP_HOST' in request.META else '' # domain=pp.mdland.com:7000
        if self._domain:
            pos = self._domain.find(':')
            if pos > 0:
                self._domain = self._domain[:pos]

    def get_domain(self):
        return self._domain

    def get_preferred_location_list(self):
        if self._domain.find('somostesting') != -1:
            return []

        res_list = []
        sql = """select cmt.CommunityID , sts.LocationID from somos_Community cmt 
                inner join somos_CommunityPreferedSite sts on cmt.CommunityID = sts.CommunityID and sts.Deleted=0
                 where DomainName = '{0}'""".format(self._domain)

        ec_db = sql_ec_db(as_dict=True)
        res_list = ec_db.fetch_sql_dict_list(sql)
        if len(res_list) > 0:
            res_list = [x['LocationID'] for x in res_list]
        print(f'domain={self._domain}, location_list={res_list}')
        return res_list

    def get_current_community(self):
        if self._domain.find('somostesting') != -1:
            return -1
        sql = """select CommunityID from somos_Community where  DomainName = '{0}' """.format(self._domain)
        ec_db = sql_ec_db(as_dict=True)
        res_list = ec_db.fetch_sql_dict_list(sql)
        return res_list[0]['CommunityID'] if len(res_list) > 0 else -1
