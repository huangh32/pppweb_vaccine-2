import datetime, os, binascii
from PhmWeb.biz.aws_ses import AwsSES
from PhmWeb.utils.dbconfig import get_PHM_db


class ViphealthWelcomeSes:

    def email_welcome_message(self, patient_first_name, email, url):
        subject = 'Welcome to VIPHealth'
        if url:
            welcome = f"As you have successfully created your account in {url}, now you are eligible to have access to our patient portal mobile app, "
        else:
            welcome = "Please download "

        content = f'''Hello {patient_first_name},<br><br><br>

    {welcome}VIPHEALTH to manage your existing COVID-19 vaccination appointment or schedule a new appointment for free.<br><br><br>



    Besides this, you can also use VIPHealth app to:<br><br><br>



    Schedule on-site and telemedicine doctor appointments<br>
    See your test results<br>
    View medication, vaccine and visit history<br>
    Send messages to your doctor(s)<br>
    And many more!<br><br><br>



    To access VIPHealth app, please search "VIPHealth" in App Store/ Google Play, or simply click the urls below For downloading:<br><br><br>



    https://play.google.com/store/apps/details?id=com.react_viphealth&hl=en_US<br><br><br>


    https://apps.apple.com/us/app/viphealth/id1265405734<br><br><br>


    <img src="https://mobile.mdland.com/ezHealthApiService/page/viphealth_download.png" /><br><br><br>


    Your VIPHealth usename is {email}<br><br><br>



    Thanks for using VIPHealth,<br>
    MDLand Support'''
        AwsSES().send_email(reciptent=email, subject=subject, content=content)

