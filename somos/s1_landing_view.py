import datetime, json, bson
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire


@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'changelanguage':
        return change_language(request)
    elif action == 'saveQuestionnaire':
        return save_questionnaire(request)
    else:
        return init_html_page(request, site_mode='PP Normal')


# /p/callcenter
def call_center(request):
    return init_html_page(request, site_mode='PP Call Center')


# show html page
def init_html_page(request, site_mode):
    page_dict = dict()
    page_dict['BookingDay'] = DateUtils.datetime_to_short_string(datetime.datetime.now())

    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()
    page_dict['SiteMode'] = site_mode
    page_dict['ClinicID'] = RequestUtils.get_clinic_id_by_host(request)

    return render(request, 'somos/questionnaire.html', page_dict)


def change_language(request):
    res = dict()
    smg = SessionManager(request)
    smg.change_session('language', RequestUtils.get_string(request, 'lang'))
    return HttpResponse(json.dumps(res), content_type='application/json')


def save_questionnaire(request):
    res_dict = dict()
    biz = IcCovidRegQuestionnaire()
    qid = biz.save_intake_form(request)
    res_dict['qid'] = qid
    res_dict['ResultCode'] = 0 if qid != '' else 1
    if qid == '':
        res_dict['Error'] = biz.get_error_info()

    return JsonResponse(res_dict)
