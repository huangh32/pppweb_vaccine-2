import datetime, json, bson
from django.shortcuts import render,  redirect
from django.http import HttpResponse, HttpResponseRedirect
from PhmWeb.biz.sessionmanager import SessionManager


def index(request):
    page_dict = dict()
    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()
    return render(request, 'somos/landing.html', page_dict)
